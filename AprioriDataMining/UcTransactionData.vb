﻿Public Class UcTransactionData
    Dim database As DatabaseEntities

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub

    Private Sub UcTransactionData_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dataGridTransaction.DataSource = database.transactions.ToList()
    End Sub

    Private Sub dataGridTransaction_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dataGridTransaction.CellContentDoubleClick

    End Sub

    Private Sub dataGridTransaction_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dataGridTransaction.CellDoubleClick
        Dim id As Integer = dataGridTransaction.Rows(e.RowIndex).Cells(0).Value

        'dataGridTransactionItem.DataSource = database.transaction_items.Where(Function(t) t.transaction_id = id).ToList
        dataGridTransactionItem.DataSource = (From item In database.transaction_items
                                              Where item.transaction_id = id
                                              Select New TransactionDetail With {
                                                 .transaction_item = item
                                                 }).ToList()
    End Sub
End Class
