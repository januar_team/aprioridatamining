﻿Public Class UcProduct
    Dim database As DatabaseEntities

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub

    Private Sub UcProduct_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        datagrid.DataSource = database.products.ToList
    End Sub
End Class
