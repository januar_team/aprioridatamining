﻿Imports System.Text

Public Class FrmLogin
    Dim database As DatabaseEntities

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
    End Sub

    Private Sub btnBatal_Click(sender As System.Object, e As System.EventArgs) Handles btnBatal.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
        If txtUsername.Text = "" Then
            MessageBox.Show("Silahkan masukkan username!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If txtPassword.Text = "" Then
            MessageBox.Show("Silahkan masukkan password!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        Dim md5 As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
        Dim hash() As Byte = md5.ComputeHash(Encoding.UTF8.GetBytes(txtPassword.Text))

        Dim password As String = BitConverter.ToString(hash).Replace("-", String.Empty).ToLower()
        Dim username As String = txtUsername.Text

        Dim user As user = (From u In database.users
                            Where u.password.Equals(password) And u.username.Equals(username)).FirstOrDefault()
        If IsNothing(user) Then
            MessageBox.Show("Username dan password Anda salah!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Me.DialogResult = DialogResult.OK
        End If

    End Sub
End Class