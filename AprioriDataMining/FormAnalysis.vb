﻿Public Class FormAnalysis
    Dim database As DatabaseEntities
    Dim iterationData As List(Of List(Of KItem))

    Dim progressDialog As ProgressWindow

    Dim associativeRule As List(Of AssociativeRule)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        database = New DatabaseEntities()
        iterationData = New List(Of List(Of KItem))
        progressDialog = New ProgressWindow()
        progressDialog.SetTittle("Processing ...")
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        Dim thread As New Threading.Thread(AddressOf Start)
        thread.Start()
        progressDialog.ShowDialog()
    End Sub

    Public Sub Start()
        progressDialog.SetIndeterminate(True)
        progressDialog.SetLabelMessage("Processing Iteration ...")
        Dim products = database.products.ToList()
        Dim transactions = database.transactions.ToList()
        Dim thresshold = inputSupport.Value
        Dim listItem = New List(Of KItem)

        For Each p In products
            Dim count As Integer = 0
            For Each t In transactions
                count += t.transaction_items.Where(Function(i) i.product_id = p.id).Count()
            Next

            Dim support = (count / transactions.Count) * 100
            If support >= thresshold Then
                Dim item As KItem = New KItem
                item.Support = support
                item.SupportCount = count
                item.Items.Add(p.id)
                listItem.Add(item)
            End If
        Next

        iterationData.Add(listItem)
        GenerateNewItemSet(listItem, transactions, thresshold)
        GenerateAssosiatifRule()

        If progressDialog.InvokeRequired Then
            progressDialog.BeginInvoke(New Action(Sub() progressDialog.Close()))
        End If
    End Sub

    Private Sub GenerateNewItemSet(ByVal kItems As List(Of KItem),
                                   ByRef transactions As List(Of transaction),
                                   ByRef thresshold As Decimal)
        Dim items As List(Of List(Of Decimal)) = New List(Of List(Of Decimal))
        Dim newKItems = New List(Of KItem)

        For i As Integer = 0 To kItems.Count - 1
            For j As Integer = i + 1 To kItems.Count - 1
                Dim it As List(Of Decimal) = New List(Of Decimal)
                If kItems(i).Items.Count = 1 Then
                    For Each p In kItems(i).Items
                        it.Add(p)
                    Next

                    For Each p In kItems(j).Items
                        it.Add(p)
                    Next
                Else
                    If Not IsMatch(kItems(i), kItems(j)) Then
                        Continue For
                    End If

                    For k As Integer = 0 To kItems(i).Items.Count - 1
                        it.Add(kItems(i).Items(k))
                    Next

                    it.Add(kItems(j).Items(kItems(j).Items.Count - 1))
                End If
                items.Add(it)
            Next
        Next

        For Each item In items
            Dim count As Integer = 0
            For Each t In transactions
                Dim check = False
                For Each p In item
                    check = (t.transaction_items.Where(Function(q) q.product_id = p)).Count > 0
                    If Not check Then
                        Exit For
                    End If
                Next
                count += IIf(check, 1, 0)
            Next

            Dim support = (count / transactions.Count) * 100
            If support >= thresshold Then
                Dim kItem As KItem = New KItem()
                kItem.SupportCount = count
                kItem.Support = support
                For Each p In item
                    kItem.Items.Add(p)
                Next
                newKItems.Add(kItem)
            End If
        Next

        If newKItems.Count > 0 Then
            iterationData.Add(newKItems)
        End If

        If newKItems.Count > 1 Then
            GenerateNewItemSet(newKItems, transactions, thresshold)
        End If

    End Sub

    Private Function IsMatch(ByVal obj1 As KItem, ByVal obj2 As KItem) As Boolean
        For i As Integer = 0 To obj1.Items.Count - 2
            If obj1.Items(i) <> obj2.Items(i) Then
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub GenerateAssosiatifRule()

        progressDialog.SetLabelMessage("Generate Associative Rule ...")

        associativeRule = New List(Of AssociativeRule)

        Dim minConfidence = inputConfidence.Value / 100
        Dim lastIteration = iterationData.Last
        Dim products = database.products.ToList
        Dim transactions = database.transactions.ToList

        For index = iterationData.Count - 1 To 1 Step -1
            For Each rule In iterationData(index)
                For i As Integer = rule.Items.Count - 1 To 0 Step -1
                    Dim item As AssociativeRule = New AssociativeRule
                    For j As Integer = 0 To rule.Items.Count - 1
                        If i <> j Then
                            item.RuleIf.Add(rule.Items(j))
                        End If
                    Next
                    item.RuleThen.Add(rule.Items(i))
                    item.SetRuleText(products)
                    If item.CountSupport(transactions, minConfidence) Then
                        associativeRule.Add(item)
                    End If

                Next
            Next
        Next

        Me.BeginInvoke(New Action(Sub()
                                      dataGrid.DataSource = associativeRule
                                  End Sub))
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        If (Not (associativeRule Is Nothing)) Then
            Dim reportFrom As ReportForm = New ReportForm(associativeRule)
            reportFrom.ShowDialog()
        End If
    End Sub
End Class