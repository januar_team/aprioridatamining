﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UcTransactionData
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dataGridTransactionItem = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dataGridTransaction = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cashier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.transaction_items = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TransactionDetailBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductnameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PriceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionitemDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dataGridTransactionItem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataGridTransaction, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransactionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransactionDetailBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Daftar Transaksi"
        '
        'dataGridTransactionItem
        '
        Me.dataGridTransactionItem.AutoGenerateColumns = False
        Me.dataGridTransactionItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGridTransactionItem.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.TransactionidDataGridViewTextBoxColumn, Me.ProductcodeDataGridViewTextBoxColumn, Me.ProductnameDataGridViewTextBoxColumn, Me.QuantityDataGridViewTextBoxColumn, Me.PriceDataGridViewTextBoxColumn, Me.AmountDataGridViewTextBoxColumn, Me.ProductidDataGridViewTextBoxColumn, Me.TransactionitemDataGridViewTextBoxColumn})
        Me.dataGridTransactionItem.DataSource = Me.TransactionDetailBindingSource
        Me.dataGridTransactionItem.Location = New System.Drawing.Point(497, 60)
        Me.dataGridTransactionItem.Name = "dataGridTransactionItem"
        Me.dataGridTransactionItem.RowTemplate.Height = 24
        Me.dataGridTransactionItem.Size = New System.Drawing.Size(467, 421)
        Me.dataGridTransactionItem.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(494, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Item Transaksi"
        '
        'dataGridTransaction
        '
        Me.dataGridTransaction.AllowUserToAddRows = False
        Me.dataGridTransaction.AllowUserToDeleteRows = False
        Me.dataGridTransaction.AutoGenerateColumns = False
        Me.dataGridTransaction.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dataGridTransaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGridTransaction.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Cashier, Me.DateDataGridViewTextBoxColumn, Me.transaction_items})
        Me.dataGridTransaction.DataSource = Me.TransactionBindingSource
        Me.dataGridTransaction.Location = New System.Drawing.Point(13, 60)
        Me.dataGridTransaction.Name = "dataGridTransaction"
        Me.dataGridTransaction.RowTemplate.Height = 24
        Me.dataGridTransaction.Size = New System.Drawing.Size(467, 421)
        Me.dataGridTransaction.TabIndex = 0
        '
        'Id
        '
        Me.Id.DataPropertyName = "id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.Width = 48
        '
        'Cashier
        '
        Me.Cashier.DataPropertyName = "cashier"
        Me.Cashier.HeaderText = "Nama Kasir"
        Me.Cashier.Name = "Cashier"
        Me.Cashier.Width = 110
        '
        'transaction_items
        '
        Me.transaction_items.DataPropertyName = "transaction_items"
        Me.transaction_items.HeaderText = "transaction_items"
        Me.transaction_items.Name = "transaction_items"
        Me.transaction_items.Visible = False
        Me.transaction_items.Width = 148
        '
        'DateDataGridViewTextBoxColumn
        '
        Me.DateDataGridViewTextBoxColumn.DataPropertyName = "date"
        Me.DateDataGridViewTextBoxColumn.HeaderText = "Tanggal"
        Me.DateDataGridViewTextBoxColumn.Name = "DateDataGridViewTextBoxColumn"
        Me.DateDataGridViewTextBoxColumn.Width = 89
        '
        'TransactionBindingSource
        '
        Me.TransactionBindingSource.DataSource = GetType(AprioriDataMining.transaction)
        '
        'TransactionDetailBindingSource
        '
        Me.TransactionDetailBindingSource.DataSource = GetType(AprioriDataMining.TransactionDetail)
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.Visible = False
        '
        'TransactionidDataGridViewTextBoxColumn
        '
        Me.TransactionidDataGridViewTextBoxColumn.DataPropertyName = "transaction_id"
        Me.TransactionidDataGridViewTextBoxColumn.HeaderText = "transaction_id"
        Me.TransactionidDataGridViewTextBoxColumn.Name = "TransactionidDataGridViewTextBoxColumn"
        Me.TransactionidDataGridViewTextBoxColumn.Visible = False
        '
        'ProductcodeDataGridViewTextBoxColumn
        '
        Me.ProductcodeDataGridViewTextBoxColumn.DataPropertyName = "product_code"
        Me.ProductcodeDataGridViewTextBoxColumn.HeaderText = "Kode"
        Me.ProductcodeDataGridViewTextBoxColumn.Name = "ProductcodeDataGridViewTextBoxColumn"
        '
        'ProductnameDataGridViewTextBoxColumn
        '
        Me.ProductnameDataGridViewTextBoxColumn.DataPropertyName = "product_name"
        Me.ProductnameDataGridViewTextBoxColumn.HeaderText = "Produk"
        Me.ProductnameDataGridViewTextBoxColumn.Name = "ProductnameDataGridViewTextBoxColumn"
        '
        'QuantityDataGridViewTextBoxColumn
        '
        Me.QuantityDataGridViewTextBoxColumn.DataPropertyName = "quantity"
        Me.QuantityDataGridViewTextBoxColumn.HeaderText = "Jumlah"
        Me.QuantityDataGridViewTextBoxColumn.Name = "QuantityDataGridViewTextBoxColumn"
        '
        'PriceDataGridViewTextBoxColumn
        '
        Me.PriceDataGridViewTextBoxColumn.DataPropertyName = "price"
        Me.PriceDataGridViewTextBoxColumn.HeaderText = "Harga"
        Me.PriceDataGridViewTextBoxColumn.Name = "PriceDataGridViewTextBoxColumn"
        '
        'AmountDataGridViewTextBoxColumn
        '
        Me.AmountDataGridViewTextBoxColumn.DataPropertyName = "amount"
        Me.AmountDataGridViewTextBoxColumn.HeaderText = "Total"
        Me.AmountDataGridViewTextBoxColumn.Name = "AmountDataGridViewTextBoxColumn"
        '
        'ProductidDataGridViewTextBoxColumn
        '
        Me.ProductidDataGridViewTextBoxColumn.DataPropertyName = "product_id"
        Me.ProductidDataGridViewTextBoxColumn.HeaderText = "product_id"
        Me.ProductidDataGridViewTextBoxColumn.Name = "ProductidDataGridViewTextBoxColumn"
        Me.ProductidDataGridViewTextBoxColumn.Visible = False
        '
        'TransactionitemDataGridViewTextBoxColumn
        '
        Me.TransactionitemDataGridViewTextBoxColumn.DataPropertyName = "transaction_item"
        Me.TransactionitemDataGridViewTextBoxColumn.HeaderText = "transaction_item"
        Me.TransactionitemDataGridViewTextBoxColumn.Name = "TransactionitemDataGridViewTextBoxColumn"
        Me.TransactionitemDataGridViewTextBoxColumn.Visible = False
        '
        'UcTransactionData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dataGridTransactionItem)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dataGridTransaction)
        Me.Name = "UcTransactionData"
        Me.Size = New System.Drawing.Size(982, 500)
        CType(Me.dataGridTransactionItem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataGridTransaction, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransactionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransactionDetailBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents dataGridTransactionItem As DataGridView
    Friend WithEvents Label2 As Label
    Friend WithEvents dataGridTransaction As DataGridView
    Friend WithEvents TransactionBindingSource As BindingSource
    Friend WithEvents Id As DataGridViewTextBoxColumn
    Friend WithEvents Cashier As DataGridViewTextBoxColumn
    Friend WithEvents DateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents transaction_items As DataGridViewTextBoxColumn
    Friend WithEvents IdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TransactionidDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductcodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductnameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents QuantityDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PriceDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AmountDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductidDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TransactionitemDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TransactionDetailBindingSource As BindingSource
End Class
