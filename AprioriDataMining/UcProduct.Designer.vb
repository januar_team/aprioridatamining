﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcProduct
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.datagrid = New System.Windows.Forms.DataGridView()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SizeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CreatedatDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpdatedatDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionitemsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.datagrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'datagrid
        '
        Me.datagrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.datagrid.AutoGenerateColumns = False
        Me.datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datagrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.CodeDataGridViewTextBoxColumn, Me.NameDataGridViewTextBoxColumn, Me.SizeDataGridViewTextBoxColumn, Me.CreatedatDataGridViewTextBoxColumn, Me.UpdatedatDataGridViewTextBoxColumn, Me.TransactionitemsDataGridViewTextBoxColumn})
        Me.datagrid.DataSource = Me.ProductBindingSource
        Me.datagrid.Location = New System.Drawing.Point(13, 16)
        Me.datagrid.Name = "datagrid"
        Me.datagrid.RowTemplate.Height = 24
        Me.datagrid.Size = New System.Drawing.Size(955, 465)
        Me.datagrid.TabIndex = 0
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        '
        'CodeDataGridViewTextBoxColumn
        '
        Me.CodeDataGridViewTextBoxColumn.DataPropertyName = "code"
        Me.CodeDataGridViewTextBoxColumn.HeaderText = "Kode"
        Me.CodeDataGridViewTextBoxColumn.Name = "CodeDataGridViewTextBoxColumn"
        '
        'NameDataGridViewTextBoxColumn
        '
        Me.NameDataGridViewTextBoxColumn.DataPropertyName = "name"
        Me.NameDataGridViewTextBoxColumn.HeaderText = "Nama"
        Me.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn"
        '
        'SizeDataGridViewTextBoxColumn
        '
        Me.SizeDataGridViewTextBoxColumn.DataPropertyName = "size"
        Me.SizeDataGridViewTextBoxColumn.HeaderText = "Ukuran"
        Me.SizeDataGridViewTextBoxColumn.Name = "SizeDataGridViewTextBoxColumn"
        '
        'CreatedatDataGridViewTextBoxColumn
        '
        Me.CreatedatDataGridViewTextBoxColumn.DataPropertyName = "created_at"
        Me.CreatedatDataGridViewTextBoxColumn.HeaderText = "created_at"
        Me.CreatedatDataGridViewTextBoxColumn.Name = "CreatedatDataGridViewTextBoxColumn"
        '
        'UpdatedatDataGridViewTextBoxColumn
        '
        Me.UpdatedatDataGridViewTextBoxColumn.DataPropertyName = "updated_at"
        Me.UpdatedatDataGridViewTextBoxColumn.HeaderText = "updated_at"
        Me.UpdatedatDataGridViewTextBoxColumn.Name = "UpdatedatDataGridViewTextBoxColumn"
        '
        'TransactionitemsDataGridViewTextBoxColumn
        '
        Me.TransactionitemsDataGridViewTextBoxColumn.DataPropertyName = "transaction_items"
        Me.TransactionitemsDataGridViewTextBoxColumn.HeaderText = "transaction_items"
        Me.TransactionitemsDataGridViewTextBoxColumn.Name = "TransactionitemsDataGridViewTextBoxColumn"
        Me.TransactionitemsDataGridViewTextBoxColumn.Visible = False
        '
        'ProductBindingSource
        '
        Me.ProductBindingSource.DataSource = GetType(AprioriDataMining.product)
        '
        'UcProduct
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.datagrid)
        Me.Name = "UcProduct"
        Me.Size = New System.Drawing.Size(982, 500)
        CType(Me.datagrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents datagrid As DataGridView
    Friend WithEvents IdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SizeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CreatedatDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UpdatedatDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TransactionitemsDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductBindingSource As BindingSource
End Class
