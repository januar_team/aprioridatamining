﻿Public Class AssociativeRule
    Public Property RuleIf As List(Of Decimal)
    Public Property RuleThen As List(Of Decimal)
    Public Property SupportAB As String
    Public Property SupportA As String
    Public Property Confidence As String
    Public Property RuleIfText As String
    Public Property RuleThenText As String

    Public Sub New()
        RuleIf = New List(Of Decimal)
        RuleThen = New List(Of Decimal)
        SupportAB = 0
        SupportA = 0
        Confidence = 0
    End Sub

    Public Sub SetRuleText(ByVal products As List(Of product))
        For Each id In RuleIf
            RuleIfText = RuleIfText & products.Where(Function(p) p.id = id).First.name & ", "
        Next

        For Each id In RuleThen
            RuleThenText = RuleThenText & products.Where(Function(p) p.id = id).First.name & ", "
        Next
    End Sub

    Public Function CountSupport(ByVal transactions As List(Of transaction), ByVal MinConfidence As Decimal) As Boolean
        Dim countAB As Integer = 0
        For Each t In transactions
            Dim check = False
            For Each p In RuleIf
                check = (t.transaction_items.Where(Function(q) q.product_id = p)).Count > 0
                If Not check Then
                    Exit For
                End If
            Next
            countAB += IIf(check, 1, 0)
        Next

        SupportAB = String.Format("{0}%", Math.Round((countAB / transactions.Count) * 100, 3))

        Dim countA = 0
        For Each t In transactions
            countA += t.transaction_items.Where(Function(i) i.product_id = RuleThen(0)).Count()
        Next

        SupportA = String.Format("{0}%", Math.Round(((countA / transactions.Count) * 100), 3))

        If (countAB / countA) > MinConfidence Then
            Confidence = String.Format("{0}%", Math.Round((countAB / countA) * 100, 3))
            Return True
        Else
            Return False
        End If
    End Function
End Class
