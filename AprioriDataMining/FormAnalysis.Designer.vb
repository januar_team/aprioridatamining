﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAnalysis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.inputSupport = New System.Windows.Forms.NumericUpDown()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.dataGrid = New System.Windows.Forms.DataGridView()
        Me.RuleIfText = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RuleThenText = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupportAB = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupportA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Confidence = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.inputConfidence = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnReport = New System.Windows.Forms.Button()
        CType(Me.inputSupport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.inputConfidence, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(140, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nilai Minimal Support"
        '
        'inputSupport
        '
        Me.inputSupport.Location = New System.Drawing.Point(241, 7)
        Me.inputSupport.Name = "inputSupport"
        Me.inputSupport.Size = New System.Drawing.Size(50, 22)
        Me.inputSupport.TabIndex = 1
        Me.inputSupport.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(627, 6)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(75, 23)
        Me.btnStart.TabIndex = 2
        Me.btnStart.Text = "Mulai"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'dataGrid
        '
        Me.dataGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RuleIfText, Me.RuleThenText, Me.SupportAB, Me.SupportA, Me.Confidence})
        Me.dataGrid.Location = New System.Drawing.Point(15, 57)
        Me.dataGrid.Name = "dataGrid"
        Me.dataGrid.RowTemplate.Height = 24
        Me.dataGrid.Size = New System.Drawing.Size(955, 484)
        Me.dataGrid.TabIndex = 3
        '
        'RuleIfText
        '
        Me.RuleIfText.DataPropertyName = "RuleIfText"
        Me.RuleIfText.HeaderText = "RuleIf"
        Me.RuleIfText.Name = "RuleIfText"
        Me.RuleIfText.Width = 300
        '
        'RuleThenText
        '
        Me.RuleThenText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.RuleThenText.DataPropertyName = "RuleThenText"
        Me.RuleThenText.HeaderText = "RuleThen"
        Me.RuleThenText.Name = "RuleThenText"
        Me.RuleThenText.Width = 99
        '
        'SupportAB
        '
        Me.SupportAB.DataPropertyName = "SupportAB"
        Me.SupportAB.HeaderText = "Support {A U B}"
        Me.SupportAB.Name = "SupportAB"
        '
        'SupportA
        '
        Me.SupportA.DataPropertyName = "SupportA"
        Me.SupportA.HeaderText = "Support {A}"
        Me.SupportA.Name = "SupportA"
        Me.SupportA.Width = 101
        '
        'Confidence
        '
        Me.Confidence.DataPropertyName = "Confidence"
        Me.Confidence.HeaderText = "Confidence"
        Me.Confidence.Name = "Confidence"
        '
        'inputConfidence
        '
        Me.inputConfidence.Location = New System.Drawing.Point(556, 7)
        Me.inputConfidence.Name = "inputConfidence"
        Me.inputConfidence.Size = New System.Drawing.Size(50, 22)
        Me.inputConfidence.TabIndex = 5
        Me.inputConfidence.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(327, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(187, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nilai Minimal Confidence (%)"
        '
        'btnReport
        '
        Me.btnReport.Location = New System.Drawing.Point(708, 6)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(75, 23)
        Me.btnReport.TabIndex = 6
        Me.btnReport.Text = "Print"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'FormAnalysis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(982, 553)
        Me.Controls.Add(Me.btnReport)
        Me.Controls.Add(Me.inputConfidence)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dataGrid)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.inputSupport)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormAnalysis"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Proses Analisis"
        CType(Me.inputSupport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.inputConfidence, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents inputSupport As NumericUpDown
    Friend WithEvents btnStart As Button
    Friend WithEvents dataGrid As DataGridView
    Friend WithEvents RuleIfText As DataGridViewTextBoxColumn
    Friend WithEvents RuleThenText As DataGridViewTextBoxColumn
    Friend WithEvents SupportAB As DataGridViewTextBoxColumn
    Friend WithEvents SupportA As DataGridViewTextBoxColumn
    Friend WithEvents Confidence As DataGridViewTextBoxColumn
    Friend WithEvents inputConfidence As NumericUpDown
    Friend WithEvents Label2 As Label
    Friend WithEvents btnReport As Button
End Class
