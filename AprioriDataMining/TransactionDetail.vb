﻿Public Class TransactionDetail
    Public Property id As Decimal
        Get
            Return transaction_item.id
        End Get
        Set(value As Decimal)
            transaction_item.id = value
        End Set
    End Property
    Public Property transaction_id As Decimal
        Get
            Return transaction_item.transaction_id
        End Get
        Set(value As Decimal)
            transaction_item.transaction_id = value
        End Set
    End Property
    Public Property product_id As Decimal
        Get
            Return transaction_item.product_id
        End Get
        Set(value As Decimal)
            transaction_item.product_id = value
        End Set
    End Property
    Public Property quantity As Integer
        Get
            Return transaction_item.quantity
        End Get
        Set(value As Integer)
            transaction_item.quantity = value
        End Set
    End Property
    Public Property price As Integer
        Get
            Return transaction_item.price
        End Get
        Set(value As Integer)
            transaction_item.price = value
        End Set
    End Property
    Public Property amount As Integer
        Get
            Return transaction_item.amount
        End Get
        Set(value As Integer)
            transaction_item.amount = value
        End Set
    End Property
    Public Property product_name As String
        Get
            Return transaction_item.product.name
        End Get
        Set(value As String)
            transaction_item.product.name = value
        End Set
    End Property
    Public Property product_code As String
        Get
            Return transaction_item.product.code
        End Get
        Set(value As String)
            transaction_item.product.code = value
        End Set
    End Property

    Public Property transaction_item As transaction_items
End Class
