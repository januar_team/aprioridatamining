﻿Imports Microsoft.Reporting.WinForms

Public Class ReportForm
    Dim datasource As List(Of AssociativeRule)

    Public Sub New(ByRef source As List(Of AssociativeRule))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.datasource = source
    End Sub

    Private Sub ReportForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.BeginInvoke(
            New Action(
            Sub()
                Dim pg = New System.Drawing.Printing.PageSettings()
                pg.Margins.Top = 0
                pg.Margins.Bottom = 0
                pg.Margins.Left = 0
                pg.Margins.Right = 0
                Me.ReportViewer1.SetPageSettings(pg)


                ReportViewer1.LocalReport.SetParameters(New ReportParameter("Date", DateTime.Now.ToString("dd/MM/yyyy")))
                AssociativeRuleBindingSource.DataSource = datasource
                Me.ReportViewer1.RefreshReport()
            End Sub))
    End Sub
End Class