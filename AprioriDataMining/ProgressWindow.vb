﻿Public Class ProgressWindow
    Public Sub UpdateProgress(ByVal progress As Integer)
        If (progressBar.InvokeRequired) Then
            progressBar.BeginInvoke(New Action(Sub() progressBar.Value = progress))
        Else
            progressBar.Value = progress
        End If
    End Sub

    Public Sub SetLabelMessage(ByVal text As String)
        If (progressBar.InvokeRequired) Then
            progressBar.BeginInvoke(New Action(Sub() lblText.Text = text))
        Else
            lblText.Text = text
        End If
    End Sub

    Public Sub SetTittle(ByVal text As String)
        If (progressBar.InvokeRequired) Then
            progressBar.BeginInvoke(New Action(Sub() Me.Text = text))
        Else
            Me.Text = text
        End If
    End Sub

    Public Sub SetIndeterminate(ByVal isIndeterminate As Boolean)
        If (progressBar.InvokeRequired) Then
            progressBar.BeginInvoke(
                New Action(Sub()
                               If (isIndeterminate) Then
                                   progressBar.Style = ProgressBarStyle.Marquee
                               Else
                                   progressBar.Style = ProgressBarStyle.Blocks
                               End If
                           End Sub
            ))
        Else
            If (isIndeterminate) Then
                progressBar.Style = ProgressBarStyle.Marquee
            Else
                progressBar.Style = ProgressBarStyle.Blocks
            End If
        End If
    End Sub
End Class