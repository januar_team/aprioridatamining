﻿Public Class KItem
    Public Property SupportCount As Integer
    Public Property Support As Double
    Public Property Items As List(Of Decimal)

    Public Sub New()
        Support = 0
        SupportCount = 0
        Items = New List(Of Decimal)
    End Sub
End Class
