﻿Public Class MainForm
    Dim ucHome As UcHomePage
    Dim ucTransaction As UcTransactionData
    Dim ucProduct As UcProduct

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Hide()
        Dim frmLogin = New FrmLogin()
        Dim result = frmLogin.ShowDialog()
        If result = DialogResult.OK Then
            Me.Show()
            ucHome = New UcHomePage()
            ucHome.Dock = DockStyle.Fill
            ucHome.Location = New Point(0, 0)
            ucHome.Visible = False
            panelMain.Controls.Add(ucHome)

            ucTransaction = New UcTransactionData()
            ucTransaction.Dock = DockStyle.Fill
            ucTransaction.Location = New Point(0, 0)
            ucTransaction.Visible = False
            panelMain.Controls.Add(ucTransaction)

            ucProduct = New UcProduct()
            ucProduct.Dock = DockStyle.Fill
            ucProduct.Location = New Point(0, 0)
            ucProduct.Visible = False
            panelMain.Controls.Add(ucProduct)

            ucHome.Visible = True
        Else
            Me.Close()
        End If
    End Sub

    Private Sub RemoveUC()
        For Each item As Control In panelMain.Controls
            If item.GetType().BaseType.FullName.Equals("System.Windows.Forms.UserControl") Then
                Dim uc As UserControl
                uc = CType(item, UserControl)
                uc.Visible = False
            End If
        Next
    End Sub

    Private Sub menuHome_Click(sender As Object, e As EventArgs) Handles menuHome.Click
        If Not ucHome.Visible Then
            RemoveUC()
            ucHome.Visible = True
        End If
    End Sub

    Private Sub menuTransaksi_Click(sender As Object, e As EventArgs) Handles DataTransaksiToolStripMenuItem.Click
        If Not ucTransaction.Visible Then
            RemoveUC()
            ucTransaction.Visible = True
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub menuBarang_Click(sender As Object, e As EventArgs) Handles DataBarangToolStripMenuItem.Click
        If Not ucProduct.Visible Then
            RemoveUC()
            ucProduct.Visible = True
        End If
    End Sub

    Private Sub AnalisaDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AnalisaDataToolStripMenuItem.Click
        Dim frmAnalysis As FormAnalysis = New FormAnalysis()
        Dim result As DialogResult = frmAnalysis.ShowDialog()
    End Sub
End Class
